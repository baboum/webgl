_.Mouse = function(elem) {

  _.Events.call(this);

  this.elem = elem || document;

  this.elem.addEventListener('mousemove', this.mousemove.bind(this), false);
  this.elem.addEventListener('mousedown', this.mousedown.bind(this), false);
  this.elem.addEventListener('mouseup', this.mouseup.bind(this), false);

  // temp
  this.elem.addEventListener('mousewheel', this.mousewheel.bind(this), false);
  this.elem.addEventListener('DOMMouseScroll', this.mousewheel.bind(this), false);

  this.elem.addEventListener('contextmenu', function(e) {
    e.preventDefault();
  });

};

_.Mouse.prototype = _.extend(Object.create(_.Events.prototype), {

  mousemove: function() {
    this.dispatch('mousemove', {});
  },

  mousedown: function() {
    this.dispatch('mousedown', {});
  },

  mouseup: function() {
    this.dispatch('mouseup', {});
  },

  mousewheel: function() {
    this.dispatch('mousewheel', {});
  },

  getOffset: function() {
    var x = 0;
    var y = 0;

    var elem = this.elem;

    do {
      x += elem.offsetX;
      y += elem.offsetY;
    }
    while (elem = elem.offsetParent);

    this.offset = {};
  }

});
