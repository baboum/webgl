_.Object = function() {

  this.parent = undefined;
  this.children = [];

  this.position = vec3.create();
  this.rotation = vec3.create();
  this.scale = vec3.create();

  this.quaternion = quat.create();
  this.matrix = mat4.create();

  this.updateMatrix();

};

_.Object.prototype = {

  traverse: function(fn) {
    for (var i = this.children.length; i--;) {
      fn.call(this.children[i]);
      this.children[i].traverse(fn);
    }

    return this;
  },

  add: function(child) {
    if (child instanceof _.Object) {
      this.children.push[child];
      child.parent = this;
    }

    return this;
  },

  remove: function() {

  },

  // glmatrix actually implements rotations by itself
  // maybe use that later
  rotate: function(a, rad) {
    quat.setAxisAngle(_.quat, a, rad);
    quat.multiply(this.quaternion, _.quat, this.quaternion);

    this.updateMatrix();

    return this;
  },

  rotateX: function(rad) {
    return this.rotate(_.X, rad);
  },

  rotateY: function(rad) {
    return this.rotate(_.Y, rad);
  },

  rotateZ: function(rad) {
    return this.rotate(_.Z, rad);
  },

  update: function() {
    this.traverse(function() {
      this.update();
    });

    return this.updateMatrix();
  },

  updateMatrix: function() {
    mat4.fromRotationTranslation(this.matrix, this.quaternion, this.position);

    return this;
  },

  // come on, you really thought i was going to clone that?
  clone: function() {
    return new _.Object;
  }

};
