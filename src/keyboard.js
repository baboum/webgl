_.Keyboard = function() {

  _.Events.call(this);

  this.keys = {};

  document.addEventListener('keydown', this.keydown.bind(this), false);
  document.addEventListener('keyup', this.keyup.bind(this), false);
  document.addEventListener('keypress', this.keypress.bind(this), false);

};

_.Keyboard.prototype = _.extend(Object.create(_.Events.prototype), {

  keycodes: {
    37: 'left',
    38: 'up',
    39: 'right',
    40: 'down',
    45: 'insert',
    46: 'delete',
    8: 'backspace',
    9: 'tab',
    13: 'enter',
    16: 'shift',
    17: 'ctrl',
    18: 'alt',
    19: 'pause',
    20: 'capslock',
    27: 'escape',
    32: 'space',
    33: 'pageup',
    34: 'pagedown',
    35: 'end',
    36: 'home',
    112: 'f1',
    113: 'f2',
    114: 'f3',
    115: 'f4',
    116: 'f5',
    117: 'f6',
    118: 'f7',
    119: 'f8',
    120: 'f9',
    121: 'f10',
    122: 'f11',
    123: 'f12',
    144: 'numlock',
    145: 'scrolllock',
    186: 'semicolon',
    187: 'equal',
    188: 'comma',
    189: 'dash',
    190: 'period',
    191: 'slash',
    192: 'graveaccent',
    219: 'openbracket',
    220: 'backslash',
    221: 'closebraket',
    222: 'singlequote'
  },

  keypress: function(e) {
    this.dispatch('mousemove', {});
  },

  keydown: function(e) {
    if (e.which > 47 && e.which < 91)
      var name = String.fromCharCode(e.which).toLowerCase();
    else
      var name = this.keycodes[e.which];

    var data = {
      key: name,
      event: e
    };

    this.keys[name] = true;

    this.dispatch('keydown', data);
  },

  keyup: function(e) {
    this.dispatch('keyup', {});

    if (e.which > 47 && e.which < 91)
      var name = String.fromCharCode(e.which).toLowerCase();
    else
      var name = this.keycodes[e.which];

    this.keys[name] = false;
  }

});
