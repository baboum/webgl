_.storage = {

  _cache: {},

  get: function(key) {
    if (!this._cache[key]) {
      this._cache[key] = localStorage(key);
    }
    return this._cache[key];
  },

  set: function(key, value) {
    this._cache[key] = value;
  },

  sync: function() {
    for (var k in this._cache) {
      if (this._cache.hasOwnProperty(k)) {
        localStorage[k] = this._cache[k];
      }
    }
  }

};
