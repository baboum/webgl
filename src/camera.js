_.Camera = function(params) {

  _.Object.call(this);

  _.extend(this, {

    fov: Math.PI / 5,
    ratio: 1,

    near: .1,
    far: 100

  }, params);

  this.perspective = mat4.create();
  this.updatePerspective();

};

_.Camera.prototype = _.extend(Object.create(_.Object.prototype), {

  setRatio: function(ratio) {
    this.ratio = ratio;
    this.updatePerspective();

    return this;
  },

  setFov: function(fov) {
    this.fov = fov;
    this.updatePerspective();

    return this;
  },

  updatePerspective: function() {
    mat4.perspective(this.perspective, this.fov, this.ratio, this.near, this.far);

    return this;
  }

});
