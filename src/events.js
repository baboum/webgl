_.Events = function() {
  this.listeners = {};
};

_.Events.prototype = {

  on: function(type, listener) {
    if (!this.listeners[type]) {
      this.listeners[type] = [];
    }

    if (this.listeners[type].indexOf(listener) === -1) {
      this.listeners[type].push(listener);
    }

    return listener;
  },

  dispatch: function(type, data) {
    var listener;

    if (this.listeners[type]) {
      for (var i = 0, c = this.listeners[type].length; i < c; i++) {
        listener = this.listeners[type][i];
        listener.call(this, data);
      }
    }
  }

};
