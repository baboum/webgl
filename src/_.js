var _ = {
  // buffer geometry tools
  quat: quat.create(),
  vec3: vec3.create(),
  mat4: mat4.create(),

  // default basis
  X: vec3.fromValues(1, 0, 0),
  Y: vec3.fromValues(0, 1, 0),
  Z: vec3.fromValues(0, 0, 1)
};

_.extend = function(a) {
  for (var i = 1; i < arguments.length; i++) {
    for (var j in arguments[i]) {
      var g = arguments[i].__lookupGetter__(j);
      var s = arguments[i].__lookupSetter__(j);
      if (g || s) {
        if (g)
          a.__defineGetter__(j, g);
        if (s)
          a.__defineSetter__(j, s);
      }
      else {
        a[j] = arguments[i][j];
      }
    }
  }
  return a;
};

_.each = function(obj, fn, ctx) {
  for (var k in obj) {
    if (!obj.hasOwnProperty(k)) continue;
    fn.call(ctx, obj[k], k, obj);
  }
  return obj;
};

_.error = function(str) {
  throw new Error(str);
};

_.log = function(str) {
  console.log(str);
};

window._ = _;
