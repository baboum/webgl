_.View = function(params) {

  _.extend(this, {

    cvs: document.createElement('canvas'),

    width: 480,
    height: 320,
    pixelRatio: window.devicePixelRatio || 1,

    programs: [],

    antialias: true

  }, params);

  var p = {
    antialias: this.antialias
  };

  this.gl = this.cvs.getContext('experimental-webgl', p)
            || this.cvs.getContext('webgl', p);

  if (!this.gl) {
    _.error('webgl not available :\'(');
    this.gl = false;
  }

  this.cvs.addEventListener('webglcontextlost', function(e) {
    e.preventDefault();
  }, false);

  // webgl params
  this.gl.enable(this.gl.CULL_FACE);
  this.gl.enable(this.gl.DEPTH_TEST);
  this.gl.clearColor(0.0, 0.0, 0.0, 1.0);

  this.resize();


  // TEMP STUFF, default geometry
  this.vertexPositionBuffer = this.gl.createBuffer();
  this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertexPositionBuffer);
  var vertices = new Float32Array([
         0.0,  1.0,  0.0,
        -1.0, -1.0,  1.0,
         1.0, -1.0,  1.0,

         0.0,  1.0,  0.0,
         1.0, -1.0,  1.0,
         1.0, -1.0, -1.0,

         0.0,  1.0,  0.0,
         1.0, -1.0, -1.0,
        -1.0, -1.0, -1.0,

         0.0,  1.0,  0.0,
        -1.0, -1.0, -1.0,
        -1.0, -1.0,  1.0,

        -1.0, -1.0, -1.0,
         1.0, -1.0,  1.0,
        -1.0, -1.0,  1.0,

        -1.0, -1.0, -1.0,
         1.0, -1.0,  -1.0,
         1.0, -1.0,  1.0,
  ]);

  this.gl.bufferData(this.gl.ARRAY_BUFFER, vertices, this.gl.STATIC_DRAW);

  this.vertexPositionBuffer.itemSize = 3;
  this.vertexPositionBuffer.numItems = 18;

  this.faceColors = this.gl.createBuffer();
  this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.faceColors);
  this.gl.bufferData(this.gl.ARRAY_BUFFER, new Uint8Array([
    255, 0, 255,
    0, 255, 255,
    255, 255, 0,

    255, 0, 255,
    255, 255, 0,
    255, 255, 255,

    255, 0, 255,
    255, 255, 255,
    255, 0, 255,

    255, 0, 255,
    255, 0, 255,
    0, 255, 255,

    255, 0, 255,
    255, 255, 0,
    0, 255, 255,

    255, 0, 255,
    255, 255, 255,
    255, 255, 0,
  ]), this.gl.STATIC_DRAW);

  this.shaderProgram = undefined;

  this.perspective = mat4.create();
  this.position = mat4.create();
};

_.View.prototype = {

  resize: function(w, h) {
    var w = Math.floor(w * this.pixelRatio);
    var w = Math.floor(w * this.pixelRatio)

    this.width = w || this.width;
    this.height = h || this.height;

    this.cvs.width = this.width;
    this.cvs.height = this.height;

    this.gl.viewport(0, 0, this.gl.drawingBufferWidth, this.gl.drawingBufferHeight);
  },

  render: function(camera) {
    this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);

    var position = mat4.create();

    mat4.translate(position, position, [0.0, 0.0, -7.0]);
    mat4.rotate(position, position, performance.now() / 500, [1.0, 0.8, 0.0]);

    mat4.invert(_.mat4, camera.matrix);
    mat4.mul(position, _.mat4, position);

    this.gl.uniformMatrix4fv(this.shaderProgram.projectionMatrix, false, camera.perspective);
    this.gl.uniformMatrix4fv(this.shaderProgram.modelViewMatrix, false, position);

    if (!this.shaderProgram) return;

    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertexPositionBuffer);
    this.gl.vertexAttribPointer(
      this.shaderProgram.positionAttribute,
      this.vertexPositionBuffer.itemSize,
      this.gl.FLOAT,
      false, 0, 0);

    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.faceColors);
    this.gl.vertexAttribPointer(this.shaderProgram.colorAttribute, 3, this.gl.UNSIGNED_BYTE, true, 0, 0);

    this.gl.drawArrays(this.gl.TRIANGLES, 0, this.vertexPositionBuffer.numItems);

    // wait
  },

  getShader: function(id) {
    var elem = document.getElementById(id);
    var str = '';
    var shader;
    var k = elem.firstChild;

    while (k) {
      if (k.nodeType == 3)
        str += k.textContent;
      k = k.nextSibling;
    }

    if (elem.type == 'x-shader/x-vertex') {
      shader = this.gl.createShader(this.gl.VERTEX_SHADER);
    }
    else if (elem.type == 'x-shader/x-fragment') {
      shader = this.gl.createShader(this.gl.FRAGMENT_SHADER);
    }

    else return null;

    this.gl.shaderSource(shader, str);
    this.gl.compileShader(shader);

    if (!this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS)) {
      _.error(this.gl.getShaderInfoLog(shader));
      return null;
    }

    return shader;
  },

  setProgram: function(vertexShader, fragmentShader) {
    this.shaderProgram = this.gl.createProgram();
    this.gl.attachShader(this.shaderProgram, vertexShader);
    this.gl.attachShader(this.shaderProgram, fragmentShader);
    this.gl.linkProgram(this.shaderProgram);

    if (!this.gl.getProgramParameter(this.shaderProgram, this.gl.LINK_STATUS)) {
      _.error('could not initialise shaders');
      this.shaderProgram = undefined;
    }

    this.gl.useProgram(this.shaderProgram);

    this.shaderProgram.positionAttribute = this.gl.getAttribLocation(this.shaderProgram, 'a_position');
    this.gl.enableVertexAttribArray(this.shaderProgram.positionAttribute);

    this.shaderProgram.colorAttribute = this.gl.getAttribLocation(this.shaderProgram, 'a_color');
    this.gl.enableVertexAttribArray(this.shaderProgram.colorAttribute);

    this.shaderProgram.projectionMatrix = this.gl.getUniformLocation(this.shaderProgram, 'u_projection');
    this.shaderProgram.modelViewMatrix = this.gl.getUniformLocation(this.shaderProgram, 'u_modelview');
  }

};
