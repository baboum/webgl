// what using the library could look like
// maybe

var world = new _.World;
var scene = new _.Scene;
var camera = new _.Camera;

// rendered objects have to be linked to the world they are put in
// i.e. the gl context they will be used with

var cube = new _.Cube(world);

scene.add(cube);

world.render(scene, camera);



// event driven

var keyboard = new _.Keyboard;
var mouse = new _.Mouse;

keyboard.on('keydown', function(e) {

  console.log('key has been pressed');

});

mouse.on('mousemove', function(e) {

  console.log('mouse has been moved');

});
